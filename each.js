const each = (array, callback) => {

  if (Array.isArray(array) && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      callback(array[index], index, array)
    }
  } else {
    throw new Error('Invalid input: Please provide a non-empty array')
  }
  
}

module.exports = each