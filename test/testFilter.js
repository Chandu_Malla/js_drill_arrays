const filter = require('../filter')

describe('filter function', () => {
  test('should filter elements based on the callback', () => {
    const testArray = [1, 2, 3, 4]
    const callback = (element) => element % 2 === 0

    const result = filter(testArray, callback)

    expect(result).toEqual([2, 4])
  })

  test('should return an empty array for no matching elements', () => {
    const testArray = [1, 2, 3, 4]
    const callback = (element) => element > 5

    const result = filter(testArray, callback)

    expect(result).toEqual([])
  })

  test('should throw an error for invalid input', () => {
    const testArray = 'Not an Array'
    const callback = (element) => element > 0

    expect(() => filter(testArray, callback)).toThrowError(
      'Invalid input: Please provide a non-empty array',
    )
  })
})
