const map = require('../map')

describe('map function', () => {
  test('should create a new array with the results of applying the callback', () => {

    const testArray = [1, 2, 3, 4]
    const callback = (element) => element * 2

    const result = map(testArray, callback)

    expect(result).toEqual([2, 4, 6, 8])
  })

  test('should return an empty array for an empty array', () => {
    const emptyArray = [];
    const callback = (element) => element * 2;

    const result = map(emptyArray, callback);

    expect(result).toEqual([]);
  });

  test('should throw an error for invalid input', () => {
    const testArray = 'Not an Array'
    const callback = (element) => element * 2

    expect(() => map(testArray, callback)).toThrowError(
      'Invalid input: Please provide a non-empty array',
    )
  })
})
