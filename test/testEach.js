const each = require('../each')

describe('each function', () => {
  test('should iterate over each element of the array and invoke the callback', () => {
    const mockCallback = jest.fn()
    const testArray = [1, 2, 3, 4]

    each(testArray, mockCallback)

    expect(mockCallback).toHaveBeenCalledTimes(testArray.length)

    testArray.forEach((element, index) => {
      expect(mockCallback).toHaveBeenCalledWith(
        element,
        index,
        testArray,
      )
    })
  })

  test('should throw an error for invalid input', () => {
    const mockCallback = jest.fn()
    const testArray = []

    expect(() => each(testArray, mockCallback)).toThrowError(
      'Invalid input: Please provide a non-empty array',
    )

    expect(mockCallback).not.toHaveBeenCalled()
  })
})
