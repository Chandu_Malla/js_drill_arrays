const reduce = require('../reduce');

describe('reduce function', () => {
  test('should reduce an array to a single value', () => {
    const inputArray = [1, 2, 3, 4];
    const callback = (accumulator, currentValue) => accumulator + currentValue;
    const initialValue = 0;
    const expectedResult = 10;

    const result = reduce(inputArray, callback, initialValue);

    expect(result).toEqual(expectedResult);
  });

  test('should reduce an array with initial value', () => {
    const inputArray = [1, 2, 3, 4];
    const callback = (accumulator, currentValue) => accumulator * currentValue;
    const initialValue = 2;
    const expectedResult = 48;

    const result = reduce(inputArray, callback, initialValue);

    expect(result).toEqual(expectedResult);
  });

  test('should throw an error for invalid input', () => {
    const invalidInput = 'not an array';
    const callback = (accumulator, currentValue) => accumulator + currentValue;
    const initialValue = 0;

    expect(() => reduce(invalidInput, callback, initialValue)).toThrowError(
      'Invalid input: Please provide a non-empty array'
    );
  });
});