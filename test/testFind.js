const find = require('../find')

describe('find function', () => {
  test('should find the first element that satisfies the callback', () => {
    const testArray = [1, 2, 3, 4]
    const callback = (element) => element > 2

    const result = find(testArray, callback)

    expect(result).toEqual(3)
  })

  test('should return undefined if no matching element is found', () => {
    const testArray = [1, 2, 3, 4]
    const callback = (element) => element > 5

    const result = find(testArray, callback)

    expect(result).toBeUndefined()
  })

  test('should throw an error for invalid input', () => {
    const testArray = 'Not an Array'
    const callback = (element) => element > 0

    expect(() => find(testArray, callback)).toThrowError(
      'Invalid input: Please provide a non-empty array',
    )
  })
})
