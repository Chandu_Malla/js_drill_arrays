const flatten = require('../flatten')

describe('flatten function', () => {
  test('should flatten a nested array', () => {
    
    const nestedArray = [1, [2, [3, 4], 5], 6, [7, 8]]
    const expectedResult = [1, 2, 3, 4, 5, 6, 7, 8]

    const result = flatten(nestedArray)

    expect(result).toEqual(expectedResult)
  })

  test('should return an empty array for an empty array', () => {
    const emptyArray = []

    const result = flatten(emptyArray)

    expect(result).toEqual([])
  })

  test('should throw an error for invalid input', () => {
    const invalidInput = 'not an array'

    expect(() => flatten(invalidInput)).toThrowError(
      'Invalid input: Please provide a non-empty array',
    )
  })
})
