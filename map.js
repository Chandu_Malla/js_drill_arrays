const map = (array, callback) => {
  const list = new Array()

  if (Array.isArray(array) && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      list.push(callback(array[index], index, array))
    }
    return list
  } else if(Array.isArray(array) && array.length === 0) {
    return [];
  } else {
    throw new Error('Invalid input: Please provide a non-empty array')
  }
}

module.exports = map
