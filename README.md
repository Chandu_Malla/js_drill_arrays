# JS_DRILLS_OBJECTS

###  Introduction of the project aim

```## JS Drill: Arrays

```js
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
/*
    Complete the following functions.
    These functions only need to work with arrays.
    A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
    The idea here is to recreate the functions from scratch BUT if you'd like,
    feel free to Re-use any of your functions you build within your other functions.
    **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
    You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating

    Name your files like so:
        each.js
        testEach.js
        map.js
        testMap.js
*/

function each(elements, cb) {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
}

function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
}

function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
}

function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
}

function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
}

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}

```

### Technologies Used

1. **Testing Framework**: Jest
  - Jest is a JavaScript testing framework with a focus on simplicity and efficiency. It is widely used for writing unit tests, ensuring the reliability and correctness of your code.
2. **Package Manager**: npm (Node Package Manager)
   - npm is the default package manager for Node.js, allowing you to easily manage and install dependencies for your projects. It simplifies the process of sharing and distributing code packages.
3. **Programming Language**: JavaScript (JS)
    - JavaScript is a versatile and widely-used programming language. In your project, JavaScript is likely used for both front-end and back-end development, providing the interactivity and functionality of your application.


### Table of Contents
- [JS\_DRILLS\_OBJECTS](#js_drills_objects)
    - [Introduction of the project aim](#introduction-of-the-project-aim)
    - [Technologies Used](#technologies-used)
    - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
- [Navigate to the project directory](#navigate-to-the-project-directory)
- [Install dependencies](#install-dependencies)
    - [Testing](#testing)
  - [Testing the solutions](#testing-the-solutions)


## Installation

Describe's the steps to install the project and its dependencies.

```bash
# Clone the repository
git clone git@gitlab.com:Chandu_Malla/js_drill_arrays.git
```

# Navigate to the project directory
```
cd js_drill_arrays
```

# Install dependencies
```
npm install
```

### Testing

Each test file in the test folder tested indivdually with custom test 
cases.

## Testing the solutions

Go through the test directory then run node command on each test

```
cd test/
```

By running the below command, which executes custom test cases & returns output, we will use jest to test the code.

```
# To run each file
npm test testEach.js
```

```
# To run all files at once
npm test
```