const filter = (array, callback) => {

  if (Array.isArray(array) && array.length > 0) {
    const filteredList = [];

    for (let index = 0; index < array.length; index++) {
      if (callback(array[index], index, array)) {
        filteredList.push(array[index]);
      }
    }

    return filteredList;
  } else {
    throw new Error('Invalid input: Please provide a non-empty array');
  }
  
};

module.exports = filter;