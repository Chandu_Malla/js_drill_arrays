const find = (array, callback) => {

  if (Array.isArray(array) && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      if (callback(array[index], index, array)) {
        return array[index]
      }
    }
    return undefined
  } else {
    throw new Error('Invalid input: Please provide a non-empty array')
  }

}

module.exports = find