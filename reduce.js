const reduce = (array, callback, accumulator) => {

  if (Array.isArray(array) && array.length > 0) {
    for (let index = 0; index < array.length; index++) {
      if (accumulator === undefined && index === 0) {
        accumulator = array[0]
        continue
      }
      accumulator = callback(accumulator, array[index], index, array)
    }
    return accumulator
  } else {
    throw Error('Invalid input: Please provide a non-empty array')
  }

}

module.exports = reduce