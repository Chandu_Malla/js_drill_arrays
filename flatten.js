const flatten = (nestedArray) => {

  if (nestedArray.length === 0) {
    return [];
  }

  if (Array.isArray(nestedArray) && nestedArray.length > 0) {
    let flat = []

    for (let index = 0; index < nestedArray.length; index++) {
      const array = nestedArray[index]
      if (Array.isArray(array)) {
        flat = flat.concat(flatten(array))
      } else {
        flat.push(array)
      }
    }
    return flat
  } else {
    throw new Error('Invalid input: Please provide a non-empty array')
  }
  
}

module.exports = flatten